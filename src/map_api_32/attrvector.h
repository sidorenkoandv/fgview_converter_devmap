﻿#pragma once

#include <vector>

#include "CRDBAPI.h"

#if !defined(AFX_TESTMAPDLG_H__E87EFCA6_BD4E_11D2_8444_00104B6388FC__INCLUDED_)
#define AFX_TESTMAPDLG_H__E87EFCA6_BD4E_11D2_8444_00104B6388FC__INCLUDED_

//Вектор атрибутов которые будут опрошены
//Если нужно добавить опрашиваемый атрибут - просто добавьте в этот вектор
const static std::vector<unsigned long> AttrVector({
    ATTR_OBJ_RECNO,
    ATTR_MAP_BORDER,
    ATTR_MAP_PARENT_RECNO,
    ATTR_MAP_PARENT,    //Имя подсети в которой содержится
    ATTR_OBJ_CLASS,     //Все девайсы класса "Map"
    ATTR_OBJ_SELECTED,  //Показывает выделен ли объект пользователем !!! int
    ATTR_MAP_LOCKED,    //всегда 0, не видел что бы он был единицей !!!int
    ATTR_MAP_STATUS,    //Например "Инфо-Белый"
// Общие
    ATTR_OBJ_NAME,      //имя
    ATTR_OBJ_TYPE,      //тип "Устройство"
    ATTR_OBJ_DESCR,     //Описание
    ATTR_MAP_ICON,      //Файл иконки
    ATTR_MAP_ADDRESS,   //Адресс ip
    ATTR_MAP_NODEGROUP, //Группа "000=Unknown"
// Атрибуты
    ATTR_MAP_EXEC,      //Исполняемая программа
    ATTR_MAP_MACADDR,   //MAC Адрес
    ATTR_MAP_POLLINT,   //Интервал опроса
    ATTR_MAP_POLLRETRY, //Повторение опроса
    ATTR_MAP_POLLTIMO,  //Время ожидания
//    Доступ|SNMP
    ATTR_MAP_READ_ACCESS_MODE,          //Режим доступа для чтения
    ATTR_MAP_READ_WRITE_ACCESS_MODE,    //Режим доступа для чтения/записи
    ATTR_MAP_ACCESS_PARM,   //длинная строка с параметрами доступа  publicpublic43gefdgdert34trgszer45t4w5grtsgserg5gsthe56je6127.0.0.10.0.0.0
    ATTR_MAP_V3_ENGINEID, // V3 Engineid
    ATTR_MAP_V3_CONTEXTNAME,    //V3 Context Name
    ATTR_MAP_V3_READ_SECNAME,   //V3 No-Auth Security Name
    ATTR_MAP_V3_READ_WRITE_SECNAME, //V3 Auth/Priv Security Name
    ATTR_MAP_V3_AUTHPASSWD,         //V3 Auth Passwd
    ATTR_MAP_V3_PRIVPASSWD,         //V3 Priv Passwd
    ATTR_SNMP_TYPE,                  //"Устройство"

    ATTR_MAP_GETCOMM,               //read community
    ATTR_MAP_SETCOMM,               //write community
    ATTR_MAP_TRAPCOMM,              //trap community

// link
    ATTR_MAP_LINK1,
    ATTR_MAP_LINK2,

//Coordinats
    ATTR_MAP_POS_X0,
    ATTR_MAP_POS_Y0

// НЕ ИСПОЛЬЗУЕМЫЕ
//   ATTR_MAP_SHOWLINKNAME,
//   ATTR_MAP_LINKTHICKNESS,
//   ATTR_EVFILT_TYPE,
//   EVFILT_TYPE_TRAPOID,
//   MENU_ENABLE_LINK,
//   ATTR_REPORT_EXPORT_LINKS
//Непонятные атрибуты, равны нулю
//    ATTR_SNMP_REQID,
//    ATTR_SNMP_NODEADDR,
//    ATTR_SNMP_NODEACCESS,
//    ATTR_SNMP_NODECOMM,
//    ATTR_SNMP_NODENAME,
//    ATTR_SNMP_NODEOBJ,
//    ATTR_SNMP_NODERECNO,
//    ATTR_SNMP_ERROR,
//    ATTR_SNMP_VARCOUNT,
//    ATTR_SNMP_VARINDEX,
//    ATTR_SNMP_VAROID,
//    ATTR_SNMP_VARINST,
//    ATTR_SNMP_VARTYPE,
//    ATTR_SNMP_VARNAME,
//    ATTR_SNMP_VAROBJ,
//    ATTR_SNMP_VARRECNO,
//    ATTR_SNMP_VARVALUE,
//    ATTR_SNMP_RETRIES,
//    ATTR_SNMP_TIMEOUT,
//    ATTR_SNMP_MODE,
//    ATTR_SNMP_TRAPSRCCOMM,
//    ATTR_SNMP_TRAPENT,
//    ATTR_SNMP_TRAPOID,
//    ATTR_SNMP_VARVALUE_STR,
//    ATTR_SNMP_NONREPEATERS,
//    ATTR_SNMP_MAXREPEATERS,
//    ATTR_SNMP_ERROR_INDEX,
//    ATTR_USER_GROUP1,
//    ATTR_USER_GROUP2,
//    REPORT_TYPE_GROUP,
//    MAP_V3_PASSWD_MINLEN,
//    ATTR_MAP_SNMPOID
});

#endif
