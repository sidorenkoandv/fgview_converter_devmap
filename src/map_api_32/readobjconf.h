﻿#pragma once

#include "resource.h"
#include <map>
#include <vector>

#include "CRDBAPI.h"

#include "attrvector.h"

/*!
 * \brief The ReadObjConf class
 * \details Класс реализует сбор и хранение карты устройств из managerQ3.2
 * Для инициализации достаточно создать объект класса,
 * доступ без логина и пароля через консоль
 */
class ReadObjConf
{
public:
    ReadObjConf();
    ~ReadObjConf();

    /// ATTR_MAP_TYPE
    std::vector<std::map<unsigned long, std::string>> m_device;
    std::vector<std::map<unsigned long, std::string>> m_subnet;
    std::vector<std::map<unsigned long, std::string>> m_goto;
    std::vector<std::map<unsigned long, std::string>> m_link;
    std::vector<std::map<unsigned long, std::string>> m_network;
    std::vector<std::map<unsigned long, std::string>> m_ring;
    std::vector<std::map<unsigned long, std::string>> m_bus;

private:
    HSESS	m_sess = nullptr;
    HCLASS	m_map = nullptr;

    void init();
};

