//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by testmap.rc
//
#define IDD_TESTMAP_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDC_DEVICES                     1000
#define IDC_NAME                        1002
#define IDC_TYPE                        1003
#define IDC_OBJICON                     1004
#define IDC_ADDRESS                     1005
#define IDC_DESCR                       1006
#define IDC_SET                         1012
#define IDC_GET                         1013
#define IDC_TABLE                       1014
#define IDC_BORDER                      1015
#define IDC_RECNO                       1016

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1017
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
