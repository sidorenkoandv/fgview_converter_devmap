﻿#include "readobjconf.h"

#include <CRDBAPI.h>
#include <iostream>
#include <string>
#include <memory>

//функция заглушка
extern "C" {
    static long eventfunc(long arg, long event_class, long event_type, long event_arg)
    {
        return 0;
    }
}

std::string cp1251_to_utf8(const char *str){
    std::string res;
    int result_u, result_c;
    result_u = MultiByteToWideChar(1251, 0, str, -1, 0, 0);
    if(!result_u ){return 0;}
    wchar_t *ures = new wchar_t[result_u];
    if(!MultiByteToWideChar(1251, 0, str, -1, ures, result_u)){
        delete[] ures;
        return 0;
    }
    result_c = WideCharToMultiByte(65001, 0, ures, -1, 0, 0, 0, 0);
    if(!result_c){
        delete [] ures;
        return 0;
    }
    char *cres = new char[result_c];
    if(!WideCharToMultiByte(65001, 0, ures, -1, cres, result_c, 0, 0)){
        delete[] cres;
        return 0;
    }
    delete[] ures;
    res.append(cres);
    delete[] cres;
    return res;
}

ReadObjConf::ReadObjConf()
{
	//login
    m_sess = dbSessLoginConsole(0, eventfunc, (long)this);
    if (!m_sess)
    {
        dbPrintLastError("login console");
        std::cout << "[Error] login console" << std::endl;
        return;
    }

    // attach to map class
    m_map = dbClassAttach(m_sess, DB_CLASS_MAP);
    if (!m_map)
    {
        dbPrintLastError("attach map");
        dbSessLogout(m_sess);
        std::cout << "[Error] attach map" << std::endl;
        m_sess = 0;
        return;
    }

    init();
}

ReadObjConf::~ReadObjConf()
{
    if (m_sess)
    {
        dbSessLogout(m_sess);
    }

    m_sess = nullptr;
}


void ReadObjConf::init()
{
    if(!m_sess || !m_map)
    {
        std::cout << "[Error] login console or attach map" << std::endl;
        return;
    }
    auto initType = [this](int type, std::vector<std::map<unsigned long, std::string>>& vect)
    {
        HDBOBJ obj;
        for (obj = dbObjectFind(m_map, ATTR_OBJ_TYPE, (char*)&type, sizeof(type));
            obj; obj = dbObjectNext(obj, ATTR_OBJ_TYPE))
        {
            vect.push_back(std::map<unsigned long, std::string>());

            if (!obj)
            {
                std::cout << "[Error] dbObjectLoad get nullptr" << std::endl;
                continue;
            }

            //Атрибуты типа int получаем отдельно
//            vect[vect.size() - 1].emplace(ATTR_OBJ_RECNO, std::to_string(dbObjectGetRecno(obj)));
//            vect[vect.size() - 1].emplace(ATTR_MAP_BORDER, std::to_string(dbAttrGetInt(obj, ATTR_MAP_BORDER)));
//            vect[vect.size() - 1].emplace(ATTR_MAP_PARENT_RECNO, std::to_string(dbAttrGetInt(obj, ATTR_MAP_PARENT_RECNO)));

            char buf[512];
            for(auto const& attr : AttrVector)
            {
                dbAttrGetStr(obj, attr, buf, sizeof(buf));
                //Если пустая строка - значит это int
                if(std::string(buf).empty())
                    vect[vect.size() - 1].emplace(attr, std::to_string(dbAttrGetInt(obj, attr)));
                else
                    vect[vect.size() - 1].emplace(attr, cp1251_to_utf8(buf));

            }
        }
    };

    dbClassLock(m_map, DB_LOCK_READ);
    initType(MAP_TYPE_DEVICE, m_device);
    initType(MAP_TYPE_SUBNET, m_subnet);
    initType(MAP_TYPE_GOTO, m_goto);
    initType(MAP_TYPE_LINK, m_link);
    initType(MAP_TYPE_NETWORK, m_network);
    initType(MAP_TYPE_RING, m_ring);
    initType(MAP_TYPE_BUS, m_bus);
    dbClassUnlock(m_map, DB_LOCK_READ);
}
