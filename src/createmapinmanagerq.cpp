﻿#include "createmapinmanagerq.h"

#include <CRDBAPI.h>
#include <NMSServerInterface.h>

#include <algorithm>
#include <conio.h>
#include <cstdlib>
#include <exception>
#include <iostream>

using namespace std;

string encryptLightServ(const string& str) {
    return str;
}

string decryptLightServ(const string& str) {
    return str;
}

CreateMapInManagetQ::CreateMapInManagetQ(string safeString)
{
    string servStr = decryptLightServ(safeString);

    //Костыль для CORBA::ORB_init
    int argc = 1;
    char* argv[1];
    argv[0] = "argv";
    auto orb = CORBA::ORB_init(argc, argv, "");
    if (CORBA::is_nil(orb))
        return;

    CORBA::Object_ptr cobj = orb->string_to_object(servStr.data());
    if (CORBA::is_nil(cobj))
        return;

    if(!cobj || cobj == nullptr)
        return;

    m_server.reset(LightServer::_narrow(cobj));
    if (m_server == nullptr)
        return;

    m_session.reset(m_server->getSession());
    if (m_session == nullptr)
        return;
}

CreateMapInManagetQ::~CreateMapInManagetQ()
{}

bool CreateMapInManagetQ::createSubNets(
        std::vector<std::map<unsigned long, string>>& map)
{
    if(!isValid())
    {
        cout << "[Error] CreateMapInManagetQ is not valid" << endl;
        return false;
    }

    if(map.empty())
    {
        cout << "[Error] Empty map" << endl;
        return false;
    }

    while(m_subnet_map.size() < map.size())
    {
        //Если предположить соблюдение порядка
        //при инициализации входного контейнера(map)
        //то достаточно одного цикла while,
        //но мапа - отсортированное дерево, поэтому это не так
        for(const auto& subnet : map)
        {
            auto it_id = subnet.find(ATTR_OBJ_RECNO);
            auto it_parent_id = subnet.find(ATTR_MAP_PARENT_RECNO);
            if(it_id == subnet.end() || it_parent_id == subnet.end())
            {
                cout << "[Error] Subnet don't have recno or parent recno" << endl;
                return false;   //иначе зацикливание в while
            }
            long id = stol(it_id->second);
            long parent_id = stol(it_parent_id->second);

            //Упростим логику и отдельно обработаем root
            if(parent_id == 0)
            {
                if(m_subnet_map.empty())
                {
                    //Получим и добавим в мапу корневую подсеть root
                    std::shared_ptr<ISubNet> root{m_server->getRoot()};
                    if(!root)
                    {
                        cout << "[Error] Create subnet root" << endl;
                        return false;
                    }
                    copyDefSettings(subnet, root.get());
                    m_subnet_map.emplace(id, root);
                }
                //повторно root
                continue;
            }

            //pair с объектом подсети(та что parent)
            auto it_parent = m_subnet_map.find(parent_id);

            //условие при котором можно создать подсеть
            if(m_subnet_map.find(id) == m_subnet_map.end() &&
               it_parent != m_subnet_map.end())
            {
                //имя создаваемой подсети
                auto name = subnet.find(ATTR_OBJ_NAME);
                if(name == subnet.end())
                {
                    cout << "[Error] Subnet don't have name" << endl;
                    return false;
                }
                //создаем подсеть
                std::shared_ptr<ISubNet> newSubnet{
                    m_server->createSubnet(name->second.data(),
                                           it_parent->second.get())
                };
                //Настройки и свойства
                copyDefSettings(subnet, newSubnet.get());

                //добавляем в m_subnet_map
                m_subnet_map.emplace(id, newSubnet);
            }
        }
    }

    return true;
}

bool CreateMapInManagetQ::createObjects(
        std::vector<std::map<unsigned long, string>>& map)
{
    if(!isValid())
    {
        cout << "[Error] CreateMapInManagetQ is not valid" << endl;
        return false;
    }

    if(map.empty())
    {
        cout << "[Error] Empty map" << endl;
        return false;
    }

    bool res = true;

    for(const auto& object : map)
    {
        auto it_id = object.find(ATTR_OBJ_RECNO);
        auto it_parent_id = object.find(ATTR_MAP_PARENT_RECNO);
        if(it_id == object.end() || it_parent_id == object.end())
        {
            cout << "[Error] Object don't have recno or parent recno" << endl;
            res = false;
            continue;
        }
        long id = stol(it_id->second);
        long parent_id = stol(it_parent_id->second);

        auto it_parent = m_subnet_map.find(parent_id);
        if(it_parent == m_subnet_map.end())
        {
            cout << "[Error] Object don't have parent subnet" << endl;
            res = false;
            continue;
        }

        auto it_name = object.find(ATTR_OBJ_NAME);
        if(it_name == object.end())
        {
            cout << "[Error] Object don't have name" << endl;
            res = false;
            continue;
        }
        std::shared_ptr<INetworkObject> newObject{
            m_server->createObject(it_name->second.data(),
                                   it_parent->second.get())
        };
        //Настройки и свойства newObject
        copyDefSettings(object, newObject.get());

        m_object_map.emplace(id, newObject);
    }

    return res;
}

bool CreateMapInManagetQ::createLinks(
        std::vector<std::map<unsigned long, string>>& mapVect)
{
    if(!isValid())
    {
        cout << "[Error] CreateMapInManagetQ is not valid" << endl;
        return false;
    }

    bool res = true;

    for(const auto& link : mapVect)
    {
        auto it_recno_link_1 = link.find(ATTR_MAP_LINK1);
        auto it_recno_link_2 = link.find(ATTR_MAP_LINK2);
        if(it_recno_link_1 == link.end() || it_recno_link_2 == link.end())
        {
            cout << "[Error] Link don't have link1 or link2" << endl;
            res = false;
            continue;
        }

        auto first = m_object_map.find(std::atol(it_recno_link_1->second.c_str()));
        auto second = m_object_map.find(std::atol(it_recno_link_2->second.c_str()));

        if(first == m_object_map.end() || second == m_object_map.end())
        {
            cout << "[Error] Can't find object link1 or link2" << endl;
            res = false;
            continue;
        }

        NetLink_var new_link = nullptr;
        auto res = m_server->createLink(first->second.get(),
                                        second->second.get(),
                                        new_link);
        if(res != 0)
        {
            cout << "[Error] Can't create link, " << res << endl;
            res = false;
            continue;
        }
        //Это было в примере, но не понятно что это делает
//        new_link->clearSubNetLinks();

        //set settings
        copyDefSettings(link, new_link, true);

        auto it_name = link.find(ATTR_OBJ_RECNO);
        if(it_name == link.end())
        {
            cout << "[Error] Can't find link name" << endl;
            res = false;
            continue;
        }
        //m_link_map.emplace(std::atol(it_name->second.c_str()), new_link);

    }

    return res;
}

bool CreateMapInManagetQ::isValid() const
{
    return m_server && m_session;
}

//далее функции для свойств copyDefSettings ###################################

snmpVersions convertSNMPVersion(const string& version)
{
    if(version.find("V1") != string::npos)
        return snmpVersions::SNMPv1;
    else if(version.find("V2c") != string::npos)
        return snmpVersions::SNMPv2c;
    else if(version.find("V3") != string::npos)
        return snmpVersions::SNMPv3;

    //default
    cout << "convertSNMPVersion default  " << version << endl;
    return snmpVersions::SNMPvDef;
    //не понятно, где это использовать
//    snmpVersions::SNMPvICMPPing
}

snmpAuthenticationProtocol getAuthProtocol(const string& str)
{
    auto it = str.find("SHA");
    if(it != string::npos)
        return snmpAuthenticationProtocol::SNMPapSHA;

    return snmpAuthenticationProtocol::SNMPapMD5;
}

//это в 3.2 отсутствует, всегда будет DES
snmpPrivacyProtocol getPrivProtocol(const string& str)
{
    auto it = str.find("AES");
    if(it != string::npos)
        return snmpPrivacyProtocol::SNMPppAES128;

    return snmpPrivacyProtocol::SNMPppDES;
}

snmpSecurityModel getSecMode(const string& str)
{
    if(str.find("Priv Auth-") != string::npos)
        return snmpSecurityModel::SNMPAuthPriv;
    else if(str.find("Auth-") != string::npos)
        return snmpSecurityModel::SNMPAuth;

    return snmpSecurityModel::SNMPnoAuth;
}

bool CreateMapInManagetQ::copyDefSettings(
        const std::map<unsigned long, string>& map,
        INetworkObject* obj, bool isLink)
{
    if(!obj)
    {
        std::cout << "[Error] copyDefSettings, obj is nullptr" << std::endl;
        return false;
    }
    unsigned int errorCount = 0;

    auto get = [map, errorCount](long attr)mutable->std::string
    {
        auto it = map.find(attr);
        if(it != map.end())
            return it->second;
        std::cout << "[Error] copyDefSettings::get() " << attr << std::endl;
        errorCount++;
        return string();
    };

    SNMPSettings_var  settings = obj->settings();

    //Общие
    obj->Label(get(ATTR_OBJ_NAME).data());
    settings->ip = get(ATTR_MAP_ADDRESS).data();
    obj->icon(get(ATTR_MAP_ICON).data());
    //нет группы
    obj->Description(get(ATTR_OBJ_DESCR).data());

    //SNMP
    string snmpMode = get(ATTR_MAP_READ_WRITE_ACCESS_MODE);
    settings->readVersion = convertSNMPVersion(snmpMode);
    settings->readCommunity = get(ATTR_MAP_GETCOMM).data();
    settings->writeCommunity = get(ATTR_MAP_SETCOMM).data();
    settings->trapCommunity = get(ATTR_MAP_TRAPCOMM).data();
    settings->secModel = getSecMode(snmpMode); //уровень безопасности
    settings->securityName = get(ATTR_MAP_V3_READ_SECNAME).data(); //имя пользователя
    settings->authProtocol = getAuthProtocol(snmpMode); //MD5
    settings->privProtocol = getPrivProtocol(snmpMode); //DES
    settings->authPassword = get(ATTR_MAP_V3_AUTHPASSWD).data(); //пароль аутентификации
    settings->privPassword = get(ATTR_MAP_V3_PRIVPASSWD).data(); //пароль шифрования
    settings->engineID = get(ATTR_MAP_V3_ENGINEID).data();
    settings->contextName = get(ATTR_MAP_V3_CONTEXTNAME).data();

    //Атрибуты
    obj->App(get(ATTR_MAP_EXEC).data());
    obj->Interval(std::atol(get(ATTR_MAP_POLLINT).data()));
    settings->timeout = atol(get(ATTR_MAP_POLLTIMO).data());

    if(!isLink)
    {
        obj->x(atol(get(ATTR_MAP_POS_X0).data()));
        obj->y(atol(get(ATTR_MAP_POS_Y0).data()));
    }

    obj->settings(settings);
    obj->Save(*m_session);

    return errorCount == 0;
}
