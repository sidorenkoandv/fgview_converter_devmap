﻿#pragma once

#include <map>
#include <memory>
#include <string>
#include <vector>

class INetworkObject;
class ISubNet;
class LightServer;
class NetLink;
class NMSSession;

class CreateMapInManagetQ
{
public:
    CreateMapInManagetQ(std::string safeString);
    ~CreateMapInManagetQ();

    /*!
     * \brief Создать подсети
     * \details одна мапа содержит характеристики одного объекта
     * \param map вектор мап подсетей из 3.2
     * key - define из cddbapi, value - значение этого атрибута
     * \return true - успешно
     */
    bool createSubNets(std::vector<std::map<unsigned long, std::string>>& map);
    /*!
     * \brief Создать объекты
     * \details одна мапа содержит характеристики одного объекта
     * \param map вектор мап объектов(девайсов) из 3.2
     * \return true - успешно
     */
    bool createObjects(std::vector<std::map<unsigned long, std::string>>& map);
    /*!
     * \brief Создать связь
     * \details одна мапа содержит характеристики одного объекта
     * \param map вектор мап объектов(линков) из 3.2
     * \return true - успешно
     */
    bool createLinks(std::vector<std::map<unsigned long, std::string>>& mapVect);

    /*!
     * \brief isValid
     * \details проверить валидоность объекта this
     * \return true - валиден
     */
    bool isValid() const;

private:
    //! ключ мапы - это ATTR_OBJ_RECNO из manager 3.2 (CRDBAPI.h)
    std::map<long, std::shared_ptr<ISubNet>> m_subnet_map;
    std::map<long, std::shared_ptr<INetworkObject>> m_object_map;
    std::map<long, std::shared_ptr<NetLink>> m_link_map;

    std::unique_ptr<LightServer> m_server;
    std::unique_ptr<NMSSession> m_session;

    /*!
     * \brief copySubnetSettings
     * \details копирует конфиги
     * \param map мапа с конфигами
     * \param subnet подсеть для установки конфигов
     * \return true - успешно
     */
    bool copyDefSettings(const std::map<unsigned long, std::string>& map,
                         INetworkObject* obj,
                         bool isLink = false);
};
