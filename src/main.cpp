﻿#include <iostream>
#include <CRDBAPI.h>
#include <conio.h>

#include "createmapinmanagerq.h"
#include "map_api_32\readobjconf.h"

#include <map>

#include <WinUser.h>

using namespace std;

bool messageBox(string title, string descr)
{
    int msgBox = MessageBox(
                NULL,
                descr.data(),
                title.data(),
                MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON1
                );

    if(msgBox == IDYES)
        return true;

    return false;
}

int main(int argc, char* argv[])
{
    ReadObjConf mapOfOldManager;

    auto res = messageBox("Добавить в managerQ?",
        "Devices : " + to_string(mapOfOldManager.m_device.size())  + "\n" +
        "Subnet  : " + to_string(mapOfOldManager.m_subnet.size())  + "\n" +
        "Goto    : " + to_string(mapOfOldManager.m_goto.size())    + "\n" +
        "Link    : " + to_string(mapOfOldManager.m_link.size())    + "\n" +
        "Network : " + to_string(mapOfOldManager.m_network.size()) + "\n" +
        "Ring    : " + to_string(mapOfOldManager.m_ring.size())    + "\n" +
        "Bus     : " + to_string(mapOfOldManager.m_bus.size())
    );

    if(!res)
        return 0;

    if(argc != 2)
    {
        cout << "Use only one argument, session" << endl;
        return 1;
    }

    CreateMapInManagetQ mapOfManagerQ(argv[1]);
    if(!mapOfManagerQ.isValid())
    {
        cout << "[Error] mapOfManagerQ is not valid" << endl;
        getch();
        return 1;
    }

    bool success =
        mapOfManagerQ.createSubNets(mapOfOldManager.m_subnet) &&
        mapOfManagerQ.createObjects(mapOfOldManager.m_device) &&
        mapOfManagerQ.createLinks(mapOfOldManager.m_link);

    if(success)
        cout << "Ok     Press Enter.." << endl;
    else
        cout << "Error     Press Enter.." << endl;
    getch();
    return 0;
}
